var path = require('path')
// var webpack = require('webpack')


module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve('dist')
  },
  devtool: 'sourcemap',
  module: {
    rules: [
      // babeljs.io => setup => webpack:
      { 
        test: /\.js$/, 
        exclude: /node_modules/, 
        loader: "babel-loader" 
      }
    ]
  }
}
