var fixtures = {
  items: [
    {
      productId: 123,
      quantity: 2
    },
    {
      productId: 456,
      quantity: 1
    }
  ],
  products: [
    {
      id: 123,
      name: 'JavaScript',
      price: 100,
      promo: true
    },
    {
      id: 456,
      name: 'EcmaScript5',
      price: 50
    }
  ]
}