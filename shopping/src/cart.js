
// export function Calculator() { }
// Calculator.prototype.calculate = function (item, product) {
//   return product.price * item.quantity
// }

export class Calculator{

  calculate(item, product) {
    return product.price * item.quantity
  }
}


export function Cart(items, products) {
  this._products = products
  this._items = items || [];
}
Cart.prototype = {

  // List Items
  listItems: function () {
    var items = []

    // var calculator = this.calculator
    var items = this._items.map(function (item) {
      // "JavaScript - 100 x 2 = 200.00 - PROMOTION"
      var product = this._products.getById(item.productId)
      var subtotal = this.calculator.calculate(item, product).toFixed(2)
      var promotion = (product.promo ? ' PROMOTION' : '')

      console.log(product.name + ' - ' + product.price + ' x ' + item.quantity + ' = ' + subtotal + promotion)

      return ({
        product: product,
        item: item,
        subtotal: subtotal,
        promotion: promotion
      })
    }.bind(this))
    
    return items
  },

  setCalculator: function (calculator) {
    if (!(calculator instanceof Calculator)) {
      throw Error('calculator must inherit from Calculator')
    }
    this.calculator = calculator
  },

  calculator: new Calculator(),

 // Add Item
  addItem: function (product, quantity) {

    var found = this._items.filter(function (item) {
      return item.productId === product.id
    })
    // Product already in this._items
    if (found.length) {
      found[0].quantity += quantity
    } else {
      this._items.push({
        productId: product.id,
        quantity: quantity
      })
    }
  },

  displayTotal: function () {

    var calculator = this.calculator
    var total = this._items.reduce(function (total, item) {
      var product = this._products.getById(item.productId)
      return total + calculator.calculate(item, product)
    }.bind(this),0)
    console.log('Total ' + total)
    
    return total;
  },

  setData: function(data){
    this._items = data
  }
}
