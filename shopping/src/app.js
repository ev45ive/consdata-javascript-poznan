import { ProductsRepository } from './repository'
import { ItemsRepository } from './repository'
import { Products } from './products'
import { Cart } from './cart'
import { Calculator } from './cart'
import { CartView } from './views/cart-view'
import { ProductsListView } from './views/products-list-view'

export class PromoCalculator extends Calculator {

  constructor(discount = 0.9) {
    super()

    if (discount)
      this.discount = discount
  }

  static DEFAULT_DISCOUNT = 0.9

  calculate(item, product) {
    // var base = Calculator.prototype.calculate.apply(this, arguments)
    var base = super.calculate(item, product)

    if (product.promo) {
      base = base * this.discount
    }

    return base
  }

}



// export function PromoCalculator(discount) {
//   if (discount)
//     this.discount = discount
// }
// PromoCalculator.prototype = Object.assign(
//   Object.create(Calculator.prototype),
//   {
//     discount: 0.9,

//     calculate: function (item, product) {
//       // super.calculate(item,product)
//       // var base = Calculator.prototype.calculate.call(this, item, product)
//       var base = Calculator.prototype.calculate.apply(this, arguments)
//       if (product.promo) {
//         base = base * this.discount
//       }
//       return base
//     }
//   })

export function App() {

  var productsRepo = new ProductsRepository()
  var itemsRepo = new ItemsRepository()
  var products = new Products([])
  var cart = new Cart([], products);

  productsRepo.fetchAll()
    .then(function (productsList) {
      products.setData(productsList)

      return itemsRepo.fetchAll()
    })
    .then(function (itemsList) {

      window.cart = cart
      cart.setData(itemsList)

      cart.setCalculator(new PromoCalculator(0.95))

      // Add Item
      cart.addItem(products.getById(123), 3)

      var cartView = new CartView(document.getElementById('cart'), cart)
      cartView.render()

      window.cartView = cartView

      var productsView = new ProductsListView(document.getElementById('products'), products.products)
      productsView.render()
    })

}