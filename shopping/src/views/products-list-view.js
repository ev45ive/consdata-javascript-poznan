
function ListView(elem, collection) {
  this._el = elem
  this._collection = collection
}
ListView.prototype = {
  ItemView: null,
  render: function () {

    var list = document.createElement('ul')

    this._collection.forEach( (item, index, all) => {
      var listItem = document.createElement('li')
      var itemView = new this.ItemView(listItem, item, index, all)
      itemView.render()
      list.append(listItem)
    });

    this._el.append(list)
  }
}

export function ProductItemView(elem, item){
  this._el = elem;
  this.item = item
}
ProductItemView.prototype = {
  render: function(){
    this._el.innerHTML = `
    <button class="add-to-cart" data-id="${this.item.id}">
      Do koszyka
    </button>
    <span>
      ${this.item.name}
    </span>`
  }
}

export function ProductsListView() {
  ListView.apply(this,arguments)
  this.ItemView = ProductItemView

  this._el.addEventListener('click',function(event){
    if(event.target.matches('.add-to-cart')){
        this.addToCart(event.target.dataset.id)
    }
  }.bind(this))
}
ProductsListView.prototype = Object.assign(
  Object.create(ListView.prototype),
{
  addToCart: function(id){
    console.log(id)
  }

})