
export function CartView(elem, cart){
  this._el = elem
  this._cart = cart 
}

CartView.prototype = {

  render: function(){
      this._el.innerText =''

      // List Items
      var items = this._cart.listItems()

      var list = document.createElement('ul')

      items.forEach(function(item) {
        var listItem = document.createElement('li')
        listItem.innerText = [
          item.product.name, 'x', 
          item.product.price,'=',
          item.subtotal
        ].join(' ')
        list.append(listItem)
      });
      
      this._el.append(list)
      
      // Display Total
      var total = this._cart.displayTotal()

      var totalElem = document.createElement('p')
      totalElem.innerText = 'Total = ' + total;
      this._el.append(totalElem)
  }
}

